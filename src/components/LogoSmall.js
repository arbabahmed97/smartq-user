import React, { Component } from "react";
import { Image } from "react-native";

export default class LogoSmall extends Component {

    constructor(props) {
        super(props)
        
    }
    render() {
        return (
            <Image
                source={require('../assets/images/SQ_logo_white.png')}
                style={{width: 110, height: 80}}
                resizeMethod='scale'
                resizeMode={'contain'}
            />
            
        )
    }
}