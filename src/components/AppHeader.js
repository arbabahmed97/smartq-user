import React, { Component } from 'react';
import { View, StatusBar, Platform, TouchableOpacity } from 'react-native';
import { Header, Left, Body, Right, Button, Icon, Title, Text } from "native-base";
import LinearGradient from 'react-native-linear-gradient';

import { Theme } from "../common/theme";
const isIos = (Platform.OS == 'ios')
export default class AppHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    StatusBar.setBarStyle('light-content', true);
    return (
      <View style={{ height: 50, marginTop: 0 }}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          style={{ flex: 1, width: '100%' }}
          colors={Theme.gradient} >

          <View style={{ alignItems: 'center', flexDirection: 'row', backgroundColor: 'transparent', flex: 1, justifyContent: 'center' }}>
            {/* <Button transparent onPress={() => this.props.navigation.goBack()} >
                  <Icon name='arrow-back' style={{color: '#fff'}} />
                </Button> */}

            <Left />

            <Title style={{ fontWeight: 'bold' }}>{this.props.title}</Title>

            <Right>
              {/* {this.props.rightComponent} */}
              <TouchableOpacity
                onPress={this.props.onPress}>
                <Icon name='power' type='Feather' style={{ color: '#fff', marginRight: 5, }} />
              </TouchableOpacity>
            </Right>

          </View>

        </LinearGradient>
      </View>
    );
  }
}
