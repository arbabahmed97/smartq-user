import React from 'react';
import { Button } from 'react-native';
import { withNavigation } from 'react-navigation';

class BookAppointmentButton extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            response: this.props.response
        };
    }
  render() {
    console.log(this.state)
    const {response} = this.state;
    console.log(response.Schedule.ScheduleDay)
    return <Button title="BookAppointment" onPress={() => { this.props.navigation.navigate("BookAppointment", {response: this.state.response})}} />;
  }
}
export default withNavigation(BookAppointmentButton);