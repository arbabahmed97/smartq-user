import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform, FlatList, TouchableOpacity, Alert } from 'react-native';
import LinearGradient from "react-native-linear-gradient";

import { Button } from "native-base";
import Preferences from "../utils/Preferences";

import { Theme } from "../common/theme";
import { Thumbnail, Icon, Row } from 'native-base';
import { observer } from 'mobx-react';
import store from '../store';
import { AsyncStorage } from 'react-native';

import { userContext } from '../store/LoginContext';

const HOME = 0;
const PROFILE = 1;
const ABOUT_US = 2;
const CONTACT = 3;
const LOGIN = 4;
const APPOINTMENTS = 5;

@observer
export default class DrawerSidebar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {},
        };

    }

    componentDidMount() {
        this._retrieveData();
    }

    _retrieveData = async () => {

        // AsyncStorage.getItem('user').then((user) => {
        //     if (user) {
        //         const item = JSON.parse(user);
        //         this.setState({ user: item });
        //         console.log('userrrr=======' , this.state.user);
        //     }

        // });
        Preferences.getLoginData()
            .then(data => {
                console.log('sideBarLoginData', data);
                if (data) {
                    this.setState({ user: data })
                }
            })
            .catch(error => {
                console.log('error', error);
            })

    }

    render() {
        const { Patient } = store
        const { user } = store
        const OPTIONS = [
            { key: HOME, title: "Home", icon: "home", type: "SimpleLineIcons", route_name: '' },
            { key: PROFILE, title: "My Profile", icon: "user", type: "AntDesign", route_name: 'MyProfile' },
            { key: APPOINTMENTS, title: "My Appointments", icon: "clockcircleo", type: "AntDesign", route_name: 'MyAppointments' },
            { key: ABOUT_US, title: "About Us", icon: "info", type: "SimpleLineIcons", route_name: '' },
            { key: CONTACT, title: "Contact Us", icon: "mail", type: "AntDesign", route_name: '' },
            { key: LOGIN, title: store.isLogin ? 'Logout' : 'Login', icon: store.isLogin ? 'logout' : 'login', type: store.isLogin ? 'SimpleLineIcons' : 'AntDesign', route_name: 'Login' },
        ]
        return (

            <View style={styles.container}>
                <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    style={styles.header}
                    colors={Theme.gradient} >

                    <View style={styles.headerContent}>
                        <Thumbnail large source={require('../assets/images/boy.png')} />
                        <Text style={styles.profileName}>{user.Name == undefined ? 'Guest' : user.Name}</Text>
                        <Text style={styles.profDetails}>{Patient ? Patient.email : 'Karachi, Pakistan'}</Text>
                    </View>

                </LinearGradient>


                <FlatList
                    data={OPTIONS}
                    renderItem={({ item }) =>
                        <TouchableOpacity onPress={() => {

                            if (store.isLogin && item.key == 4) {
                                store.isLogin = false
                                store.user = {}
                                Preferences.saveLoginData('')
                                this.forceUpdate()
                                Alert.alert(
                                    "You have logged Out!",
                                  )
                            }
                            else {
                                this.props.navigation.navigate(item.route_name, { onLoginSuccess: this.onLoginSuccess.bind(this) })
                            }
                        }}>
                            <Row style={{ flex: 1, padding: 10 }}>
                                <Icon name={item.icon} type={item.type} style={styles.icon} />
                                <Text style={styles.title}>{item.title}</Text>
                            </Row>
                        </TouchableOpacity>
                    } />

            </View>
        );
    }

    onLoginSuccess() {
        this.forceUpdate()
    }

}


const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        paddingTop: (Platform.OS == 'ios') ? 20 : 0,
        width: 280,
        height: '100%'
    },
    header: {
        height: 200,
        width: '100%'
    },
    headerContent: {
        width: '100%',
        height: 200,
        justifyContent: 'center',
        alignItems: 'center',
    },
    profileName: {
        color: "#fff",
        fontSize: 18,
        fontWeight: '500',
        marginTop: 10,
    },
    profDetails: {
        color: "#fff",
        fontSize: 12,
        // fontWeight: '500',
        marginTop: 5,
    },
    icon: {
        color: 'gray',
        padding: 10,
        marginLeft: 10,
    },
    title: {
        flex: 1,
        alignSelf: 'center',
        color: 'gray',
    },
})


