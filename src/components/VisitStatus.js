import React, { Component } from 'react';
import { View, StatusBar, Platform, TouchableOpacity } from 'react-native';
import { Header, Left, Body, Right, Button, Icon, Title, Text, Picker } from "native-base";
import LinearGradient from 'react-native-linear-gradient';

import { Theme } from "../common/theme";
const isIos = (Platform.OS == 'ios')
export default class VisitStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      schPicker: [
        {
          id: 2,
          status: 'Rescheduled',
          code: 'RSC'
        },
        {
          id: 3,
          status: 'Cancelled',
          code: 'CNL'
        },
        {
          id: 4,
          status: 'Check-in',
          code: 'CHN'
        }
      ],
      checkInPicker: [
        {
          id: 5,
          status: 'Service Started',
          code: 'SST'
        },
        {
          id: 6,
          status: 'Check-Out',
          code: 'CHK'
        },
      ],

      checkout : [
        {
          id: 6,
          status: 'Check-Out',
          code: 'CHK'
        },
      ],
      selectedStatus: ''
    };
  }

  render() {
    //StatusBar.setBarStyle('light-content', true);
    const { Label, id, onPress,  } = this.props
    var arr = []

    if (id == 1) {
      arr = this.state.schPicker
    }
    else if (id == 4) {
      arr = this.state.checkInPicker

    }else if(id == 5) {
      arr = this.state.checkout
    }else {
      return null
    }

    return(
      <Picker
        note
        selectedValue={this.state.selectedStatus}
        style={{ height: 50, flex: 1, color: '#808080' }}
        onValueChange={(itemValue, itemIndex) => {
          this.setState({selectedStatus: itemValue})
          onPress(itemValue)
        } }>
        <Picker.Item label="Select status" value="" />
        {arr.map((site, index) => {
          return <Picker.Item key={"i_" + index} label={site.status} value={site.id} />
        })}
      </Picker>
    )

  }
}
