import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Button, Text} from "native-base";
import LinearGradient from 'react-native-linear-gradient';

import { Theme } from "../common/theme";

export default class GradientButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const {containerStyle, innerStyle, textStyle, title, isLoading} = this.props;
    return (
        <View 
        style={{
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 8,
            },
            shadowRadius: 5,
            shadowOpacity: 0.3,
            elevation: 20,
            outlineProvider: 'bounds',
            backgroundColor: "#0000",
        }}>
            <LinearGradient 
                start={{x: 0, y: 0}} 
                end={{x: 1, y: 0}}
                style={[styles.searchBtnWrapper, containerStyle]}
                colors={Theme.gradient} >
                <TouchableOpacity 
                  onPress={this.props.onPress.bind(this)}
                  style={[styles.searchBtn, innerStyle]}>
                    {isLoading 
                    ? <ActivityIndicator size="small" color="#fff" style={[textStyle, {alignSelf: 'center',}]} /> 
                    : <Text style={[styles.seachText, textStyle]}>{this.props.title}</Text>
                    }
                </TouchableOpacity>
            </LinearGradient>

            
        </View>
    );

  }


}

const styles = StyleSheet.create({
    searchBtnWrapper: {
        width: '100%',
        borderRadius: 5,
    },
    searchBtn: {
      // backgroundColor: 'red', 
      width: '100%', 
      alignContent: 'center',
      shadowColor: 'red',
      justifyContent: 'center',
      //alignContent: 'center',
    },
    seachText: {
      color: '#fff',
      fontWeight: 'bold',
      alignSelf: 'center',
      //flex: 1,
      textAlign: 'center',
    },
});
