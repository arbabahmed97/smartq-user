export const Constants = {
    MY_PROFILE: "My Profile",
    HOME: "Home",
    months: [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    ]
}