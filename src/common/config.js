import React from "react";
import { Icon } from "native-base";

import { Constants } from "./constants";
import { Theme } from "./theme";

export const Config = {
    tabOptions: {
        defaultNavigationOptions: ({ navigation }) => ({
          tabBarIcon: ({ focused, horizontal, tintColor }) => {
            const { routeName } = navigation.state;
            // let IconComponent = Ionicons;
            if (routeName === Constants.HOME) {
              iconName = `home`;
              // Sometimes we want to add badges to some icons. 
              // You can check the implementation below.
              // IconComponent = HomeIconWithBadge; 
            } 
            else {
              iconName = `user`;
            }
    
            // You can return any component that you like here!
            return <Icon name={iconName} type="AntDesign" style={{fontSize: 20, color: tintColor}}/>;
          },
        }),
        tabBarOptions: {
          activeTintColor: Theme.ColorPrimary,
          inactiveTintColor: 'gray',
        },
    },  // tab-config ends here

    // other config-elements here
}