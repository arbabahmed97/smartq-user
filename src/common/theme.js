import { StyleSheet } from "react-native";

// const ColorPrimary = '#1FBAE3'    
// const GradientInner = '#22C4D7'
// const ColorAccent = '#22C8BC'    

// const ColorPrimary = '#20789D'
// const GradientInner = '#2FA5A1'
// const ColorAccent = '#3DCCA6'

const ColorPrimary = '#2A83C5'
const GradientInner = '#69AEA4'
const ColorAccent = '#ABD778'

const styles = StyleSheet.create({
    inputWrapper: {
        borderWidth: 1,
        borderColor: '#cfcfcf',
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 8,
        marginBottom: 15,
        marginTop: 10
    },
    inputTxt: {
        flex: 1,
        color: 'gray',
    },
    inputIcon: {
        color: '#cfcfcf',
        alignSelf: 'center',
    },
})

export const Theme = {
    // ColorPrimary: '#3C4498',    // blue
    // ColorAccent : '#90C948',    // green
    ColorPrimary: ColorPrimary,    // blue
    ColorAccent : ColorAccent,    // green
    BackgroundLight: '#FCFCFF',  // off-white
    gradient: [
        // Theme.ColorPrimary, Theme.ColorPrimary, 
        // '#5D8DF3', Theme.ColorPrimary, 
        // Theme.ColorAccent,
        // '#20789D',
        // '#2FA5A1',
        // '#3DCCA6',

        ColorPrimary,
        GradientInner,
        ColorAccent,
    ],
    commonStyle: styles,
}

