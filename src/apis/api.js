import RestClient from 'react-native-rest-client';
import Preferences from "../utils/Preferences";
import { BASE_URL } from '../utils/Constant';
 
export default class Api extends RestClient {
  constructor (authToken) {
    super(`${BASE_URL}/api`, {
      headers: {
        // Include as many custom headers as you need
        Authorization: `Bearer ${authToken}`
      },
       devMode: true,
       simulatedDelay: 3000
    });
  }

  register(user){
    return this.POST('/Patient/Register', user)
  }

  login (data) {
    return this.POST('/TokenAuth/Authenticate', data)
    .then(response => {
      // this method is called from login & splash-screen, 
      // both have to save the new data so that's why it is implemented here
      if (response.success) {
        Preferences.saveUserSession(response.result);
      }
      // return the response there
      return response
    });
  }

  getSpecialities () {
    return this.GET('/Speciality/GetAll');
  }

  getDoctors (specialityId, docName) {
    return this.GET(`/Employee/GetDoctors?SpecialtyId=${specialityId}&Name=${docName}`);
  }

  getDoctorProfile (doctorId) {
    return this.GET('/Employee/GetDoctorById/' + doctorId);
  }
};