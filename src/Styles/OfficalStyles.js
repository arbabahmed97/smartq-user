import {StyleSheet} from 'react-native'

const OfficalStyles = StyleSheet.create({
    fill: {
        color : 'red'
    },
    rowContent: {
        width: "90%",
        alignSelf: "flex-end",
        paddingBottom: 10
    }
})

export default OfficalStyles;