import {StyleSheet, Dimensions} from 'react-native'
import { Theme } from "../common/theme";

const cardSize = 3.5;


const NUMBER_OF_COLUMNS = 3;
const screen = Dimensions.get("screen");
const screenWidth = screen.width;
let thumbSize = screenWidth / NUMBER_OF_COLUMNS - 10;
thumbSize = thumbSize > 200 ? 200 : thumbSize;

const HEADER_MAX_HEIGHT = Math.min(screenWidth / 1.6, 450);
const HEADER_MIN_HEIGHT = Platform.OS === "ios" ? 60 : 70; // header size when full scroll
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

const styles = StyleSheet.create({
    fill: {
      flex: 1
    },
    content: {
      flex: 1,
      width: "100%"
    },
    header: {
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      backgroundColor: "#03A9F4",
      overflow: "hidden",
      height: HEADER_MAX_HEIGHT
    },
    backgroundImage: {
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      width: null,
      height: HEADER_MAX_HEIGHT,
      resizeMode: "cover"
    },
    bar: {
      backgroundColor: "transparent",
      // marginTop: Platform.OS === 'ios' ? 28 : 38,
      marginTop: Platform.OS === "ios" ? 28 : 30, // HEADER CONTENT POSITION, BACKBTN & DOC NAME
      height: 60,
      alignItems: "center",
      justifyContent: "center",
      position: "absolute",
      top: 0,
      left: 0,
      right: 0
    },
    title: {
      color: "white",
      fontSize: 18
    },
    scrollViewContent: {
      // iOS uses content inset, which acts like padding.
      paddingTop: Platform.OS !== "ios" ? HEADER_MAX_HEIGHT : 0,
      // width: '90%',
      justifyContent: "center",
      alignItems: "center",
      alignContent: "center",
      // maxWidth: 500,
      backgroundColor: Theme.BackgroundLight
    },
    // row: {
    //     height: 40,
    //     margin: 16,
    //     backgroundColor: '#D3D3D3',
    //     alignItems: 'center',
    //     justifyContent: 'center',
    // },
    link: {
      color: "#004ed0",
      // padding: 10,
      textDecorationLine: "underline"
      // marginLeft: 15,
      // backgroundColor: 'red'
    },
    textLightSm: {
      fontSize: 10,
      color: "gray"
    },
  
    contain: {
      backgroundColor: Theme.BackgroundLight,
      flex: 1
    },
    title: {
      fontSize: 18,
      fontWeight: "bold",
      marginTop: 20
    },
    textLight: {
      color: "#cfcfcf",
      fontSize: 11,
      alignSelf: "center",
      marginRight: 5
    },
    textDark: {
      fontSize: 13
    },
    card: {
      width: cardSize,
      height: cardSize,
      alignSelf: "center",
      justifyContent: "center",
      alignItems: "center",
      alignContent: "center",
      marginLeft: "4%",
      marginRight: "4%",
      marginTop: 20,
      // borderColor: Theme,
      shadowColor: Theme.ColorPrimary
    },
    cardBody: {
      padding: "2%",
      alignItems: "center",
      justifyContent: "center"
    },
    categ_img: {
      width: cardSize / 3.5,
      height: cardSize / 3.5
      // backgroundColor: 'red',
    },
    changeButton: {
      // height: 20,
      borderRadius: 15
      // backgroundColor: 'transparent',
    },
    whiteText: {
      color: "#fff",
      marginBottom: 3
    },
    cell: {
      padding: 15,
      justifyContent: "center",
      alignItems: "center",
      borderWidth: 0.2,
      borderColor: "gray",
      flex: 1
    },
    cellText: {
      color: "gray"
    },
    cellTextWrapper: {
      marginLeft: 15,
      alignSelf: "center",
      // padding: 10,
      // paddingBottom: 10,
      flex: 1
    },
    cellTextBold: {
      color: "gray",
      fontWeight: "bold"
    },
    textHeading: {
      color: "gray",
      fontWeight: "bold",
      paddingTop: 20,
      paddingBottom: 20,
      width: "90%"
      // fontSize: 14
    },
    textContent: {
      color: "gray",
      fontWeight: "normal",
      paddingTop: 10,
      paddingBottom: 0,
      width: "90%",
      flex: 1
      // fontSize: 14
    },
    icon: {
      color: "gray",
      paddingRight: "5%",
      paddingTop: "5%"
    },
    row: {
      width: "100%",
      alignSelf: "center",
      justifyContent: "center"
    },
    rowContent: {
      width: "90%",
      alignSelf: "flex-end",
      paddingBottom: 10
    },
    redText: {
      color: "#E90E3C",
      textTransform: "uppercase",
      paddingTop: 10
    },
    transparentBtn: {
      marginRight: 20
    }
  });

  export default styles;