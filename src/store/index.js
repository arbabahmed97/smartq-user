import { observable, decorate } from "mobx";

class Store {
    /**
     * Patient Details: ID, Name, Email, City
     */
    //@observable Patient = undefined
    isLogin = false
    user = {}
    bookAppointment = false

}
decorate(Store, {
    isLogin: observable,
    user: observable,
    bookAppointment: observable,
});


export default new Store()