import React, { Component } from 'react'
import {
    View,
    // Text,
    FlatList,
    StyleSheet,
    TouchableOpacity,
    StatusBar,
    SafeAreaView,
    ActivityIndicator,
    Image,
    Alert
} from "react-native";
import {
    Container,
    Content,
    Card,
    CardItem,
    Body,
    Row,
    Thumbnail,
    Icon,
    Text,
    Button,
    Input,
    Item,
    Tabs,
    Tab,
    Header,
    Right,
    Toast,
    Root,
    Drawer,

} from "native-base";

import SideBar from "../screens/SideBar"
import AppHeader from "../components/AppHeader";
import { Theme } from "../common/theme";
import store from "../store";
import Preferences from "../utils/Preferences";
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import { BASE_URL } from "../utils/Constant";
import LinearGradient from 'react-native-linear-gradient';
import VisitStatus from '../components/VisitStatus'

// @observer
export default class Appointments extends Component {

    constructor(props) {
        super(props)

        this.state = {
            todayAppList: [],
            isLoading: false,
            refreshing: false
        }

        this.originalItem = null
    }

    async componentDidMount() {

        this.siteId = await AsyncStorage.getItem('siteId')
        console.log('siteId==', this.siteId)

        this.getTodayAppointments()
    }

    onLogout = () => {
        Alert.alert(
            'Logout',
            'Are you sure you want to logout?',
            [
                {
                    text: 'No',
                    style: 'cancel'
                },
                {
                    text: 'YES', onPress: () => {
                        store.isLogin = false
                        store.user = {}
                        Preferences.saveLoginData('')
                        setTimeout(() => {
                            Toast.show({
                                text: 'You have logged out !',
                                buttonText: "Okay",
                            });
                        }, 500);
                        this.props.navigation.navigate('Login')
                    }
                }
            ],
            { cancelable: false }
        );
    }

    getTodayAppointments = () => {

        try {
            var url = ''
            {
                this.state.refreshing ?
                    url = 'http://51.77.6.84:5200/api/OutpatientSch/GetAppointmentsForSite?SiteId=' + this.siteId + '&AppDateFrom=' + JSON.stringify(moment().startOf('day').format('YYYY-MM-DD HH:mm:ss')) + '&AppDateTo=' + JSON.stringify(moment().endOf('day').format('YYYY-MM-DD HH:mm:ss'))
                    :
                    this.setState({ isLoading: true })
                url = 'http://51.77.6.84:5200/api/OutpatientSch/GetAppointmentsForSite?SiteId=' + this.siteId + '&AppDateFrom=' + JSON.stringify(moment().startOf('day').format('YYYY-MM-DD HH:mm:ss')) + '&AppDateTo=' + JSON.stringify(moment().endOf('day').format('YYYY-MM-DD HH:mm:ss'))
            }
            console.log('url====', url)
            fetch(url)
                .then(responce => responce.json())
                .then(appointments => {
                    console.log('-----------', appointments)
                    this.setState({ isLoading: false, refreshing: false })
                    if (appointments.length > 0) {

                        let itemsToRemove = []

                        appointments.sort((a, b) => (a.TokenNo > b.TokenNo) ? 1 : -1)
                        appointments.map((app, index) => {

                            if (app.CurrentVisitStatus.Id == 2 || app.CurrentVisitStatus.Id == 3 || app.CurrentVisitStatus.Id == 6) {
                                itemsToRemove.push(app)
                            }
                        })
                        const filteredItems = appointments.filter(item => !itemsToRemove.includes(item))
                        this.setState({ todayAppList: filteredItems })
                    }
                    else {
                        this.setState({ noData: true })
                    }
                })
        } catch (error) {
            // Error saving data
            this.setState({ isLoading: false, refreshing: false })
            alert(error)
        }
    };

    converTime = (time) => {
        let hour = (time.split(':'))[0]
        let min = (time.split(':'))[1]
        let part = hour >= 12 ? 'PM' : 'AM';

        min = (min + '').length == 1 ? `0${min}` : min;
        hour = hour > 12 ? hour - 12 : hour;
        hour = (hour + '').length == 1 ? `0${hour}` : hour;

        return (`${hour}:${min} ${part}`)
    }

    onRefresh() {
        this.setState({ refreshing: true }, () => { this.getTodayAppointments(); });
    }

    changeStatus = (appId, visitStatusId, currentVisitStatus) => {
        try {
            fetch("http://51.77.6.84:5200/api/OutpatientSch/ChangeVisitStatus", {
                method: 'post',
                headers: { 'Content-type': 'application/json' },
                body: JSON.stringify({
                    AppointmentId: appId,
                    VisitStatusId: visitStatusId,
                })
            }).then(responce => responce.json()).then(user => {
                console.log('response====', user)

                if (user.Success == true) {
                    Toast.show({
                        text: 'Status has been successfully changed !',
                        buttonText: "Okay",
                    });
                } else {
                    // this.originalItem.CurrentVisitStatus['Id'] = currentVisitStatus.Id
                    // this.originalItem.CurrentVisitStatus['Name'] = currentVisitStatus.Name

                    this.setState({ reload: true })
                    Alert.alert(
                        user.Error,
                    )
                }
            })
        } catch (error) {
            // Error saving data
            this.setState({ isLoading: false })
            console.log('error', error)
            Alert.alert("Error", "Login failed")
        }
    }

    removeItem(id) {
        let arr = this.state.todayAppList.filter(function (value) {
            console.log(value.Id)
            return value.Id !== id
        })


        this.setState({ todayAppList: arr });
    }

    closeDrawer() {
        this.drawer._root.close();
      }
    
      openDrawer() {
        this.drawer._root.open();
      }
    

    render() {
        return (
            <Drawer
                ref={(ref) => {
                    this.drawer = ref;
                }}
                content={<SideBar navigation={this.props.navigation} />}
                onClose={() => this.closeDrawer()}>

                <Root>
                    <Container>

                        <StatusBar barStyle='light-content' backgroundColor={Theme.ColorPrimary} />

                        <AppHeader
                            title={"SmartQ Admin"} // TODO: change this
                            onPress={() => {
                                this.drawer._root.open();
                            }}
                        />

                        <View style={{ marginTop: 15, alignItems: 'center', flexDirection: 'row', width: '100%', justifyContent: 'center' }}>
                            <View style={{ height: 1.5, backgroundColor: '#808080', width: '25%' }} />
                            <Text style={{ fontWeight: 'bold', marginHorizontal: 5, color: Theme.ColorPrimary }}>TODAY'S APPOINTMENTS</Text>
                            <View style={{ height: 1.5, backgroundColor: '#808080', width: '25%' }} />
                        </View>

                        {this.state.todayAppList.length > 0 &&
                            <FlatList
                                style={{ marginTop: 10 }}
                                data={this.state.todayAppList}
                                renderItem={this.renderItem.bind(this)}
                                keyExtractor={(item, index) => index.toString()}
                                onRefresh={() => this.onRefresh()}
                                refreshing={this.state.refreshing}
                            />
                        }


                        {this.state.noData &&
                            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                                <Text>No Appointments yet</Text>
                            </View>
                        }

                        {this.state.isLoading &&
                            <ActivityIndicator size={'large'} color={Theme.ColorPrimary} style={{ position: 'absolute', left: 0, right: 0, top: 0, bottom: 0 }} />
                        }
                    </Container>
                </Root>

            </Drawer>

        )
    }


    renderItem({ item }) {

        let firstChar = item.Patient == null ? '0' : item.Patient.Name[0].charAt(0).toUpperCase()
        let secondChar = item.Patient == null ? '0' : item.Patient.LastName[0].charAt(0).toUpperCase()

        let itemCopy = item
        this.originalItem = item

        return (
            <View style={[styles.card]}>
                <TouchableOpacity
                >
                    <Card>
                        <CardItem>
                            <Body>
                                <Row>

                                    <View
                                        style={{
                                            justifyContent: "center",
                                            alignItems: "center",
                                            marginRight: 8,
                                        }}
                                    >
                                        {item.Patient == null || item.Patient.Photograph == null ?
                                            <LinearGradient
                                                start={{ x: 0, y: 0 }}
                                                end={{ x: 1, y: 0 }}
                                                //style={[styles.searchBtnWrapper, containerStyle]}
                                                style={{ borderRadius: 150 / 2 }}
                                                colors={Theme.gradient} >
                                                <View style={{ width: 65, height: 65, justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={{ color: '#fff', fontSize: 20 }}>{firstChar + secondChar}</Text>
                                                </View>
                                            </LinearGradient>
                                            :
                                            //<Thumbnail source={{ uri: content.img }} />
                                            <Image source={{ uri: BASE_URL + item.Patient.Photograph }} style={{ width: 75, height: 75, resizeMode: 'cover', borderRadius: 50 }} />
                                        }
                                    </View>

                                    <View>
                                        <Text style={{ fontStyle: 'italic', fontWeight: 'bold', fontSize: 17 }}>{item.Patient == null ? 'Unknown' : item.Patient.Name + ' ' + item.Patient.LastName}</Text>

                                        <Text>{item.Patient == null ? 'MRN' : item.Patient.MRNo}</Text>
                                        <Text style={{}}>TokenNo
                                          <Text style={{ fontWeight: 'bold' }}>{" "}{item.TokenNo}</Text>
                                        </Text>

                                        <View style={{ alignItems: 'center', flexDirection: 'row', marginTop: 3, width: '100%' }}>
                                            <Icon
                                                name='access-time'
                                                type='MaterialIcons'
                                                style={{
                                                    fontSize: 17,
                                                }} />

                                            <Text style={{ marginLeft: 5, fontSize: 15 }}>{this.converTime(item.StartTime)}</Text>
                                            <Text style={{ color: Theme.ColorAccent, fontSize: 15, marginLeft: 10 }}>{item.CurrentVisitStatus.Name}</Text>
                                        </View>

                                        <VisitStatus id={item.CurrentVisitStatus.Id}
                                            onPress={(id) => {

                                                console.log('---------', id)

                                                let label = ""
                                                let shouldRemove = false

                                                if (id == 2) {
                                                    label = "Rescheduled"
                                                    shouldRemove = true
                                                } else if (id == 3) {
                                                    label = "Cancelled"
                                                    shouldRemove = true

                                                } else if (id == 4) {
                                                    label = "Check-in"

                                                    item.CurrentVisitStatus['Id'] = 4

                                                } else if (id == 5) {
                                                    label = 'Service Started'
                                                    item.CurrentVisitStatus['Id'] = 5
                                                }
                                                else if (id == 6) {
                                                    label = 'Checkout'
                                                    item.CurrentVisitStatus['Id'] = 6
                                                }


                                                item.CurrentVisitStatus['Name'] = label


                                                if (shouldRemove) {
                                                    item.CurrentVisitStatus['Id'] = undefined
                                                    this.removeItem(item.Id)
                                                }

                                                this.setState({
                                                    reload: true
                                                })


                                                this.changeStatus(item.Id, id, itemCopy.CurrentVisitStatus)


                                            }} />
                                    </View>

                                </Row>
                            </Body>
                        </CardItem>
                    </Card>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        padding: 10,
        marginLeft: 10,
        marginRight: 10,
        // marginTop: 10,
    },
})
