import {
  Button,
  Container,
  Content,
  Icon,
  Input,
  Text,
  Toast,
  Header,
  Root,
  Picker
} from "native-base";
import React, { Component } from "react";
import { SafeAreaView, StyleSheet, View, Alert, Image, StatusBar } from "react-native";
import { Theme } from "../common/theme";
import GradientButton from "../components/GradientButton";
import Api from "../apis/api";
import Preferences from "../utils/Preferences";
import { userContext } from '../store/LoginContext';
//import { AsyncStorage } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import store from "../store";
import Fontisto from 'react-native-vector-icons/Fontisto'

const api = new Api("");
export default class Login extends Component {
  constructor(props) {
    super(props);
    //this.siteId = this.props.navigation.getParam("siteId", "");
    this.state = {
      emailFocused: false,
      passwordFocused: false,
      isLoading: false,
      email: "admin",
      password: "abcd1234",
      user: {},
      site: '',
      sites: []
    };
  }

  componentDidMount() {
    //console.log('=======', this.siteId)
    this.getSites()
  }

  async getSites() {

    fetch('http://51.77.6.84:5200/api/Site/GetAllSites')
      .then(response => response.json())
      .then((resp) => {
        //console.log(JSON.stringify(resp))

        if (resp.length > 0) {
          this.setState({ sites: resp })
        }
      })
      .catch((error) => {
        console.log(JSON.stringify(error))
      })
  }

  _storeData = async () => {
    try {
      await AsyncStorage.setItem(
        'user', JSON.stringify(this.state.user)
      );
    } catch (error) {
      console.log(error)
    }
  };


  OnSubmit = () => {
    const { email, password, site_id } = this.state

    if (email.trim().length < 1) {
      alert("Please enter a valid email address");
      return;
    }

    if (password.trim().length < 1) {
      alert("Please enter your password");
      return;
    }

    if (site_id == undefined) {
      alert("Please select site");
      return;
    }

    try {
      this.setState({ isLoading: true })
      fetch("http://51.77.6.84:5200/api/Employee/Login", {
        method: 'post',
        headers: { 'Content-type': 'application/json' },
        body: JSON.stringify({
          LoginName: email,
          loginPassword: password,
          SiteId: site_id
        })
      }).then(responce => responce.json()).then(user => {
        console.log('loginData====', user)
        this.setState({ isLoading: false })

        if (user.Success == true) {
          this.setState({
            email: '',
            password: '',
            site: ''
          });
          Preferences.saveLoginData(user);
          store.isLogin = true
          store.user = user
          AsyncStorage.setItem('siteId', JSON.stringify(site_id))
          
          setTimeout(() => {
            this.show('You have logged in !');
          }, 500);
          this.props.navigation.navigate('Home');

          //this._storeData()

        } else {
          Alert.alert(
            user.Error,
          )
        }
      })
    } catch (error) {
      // Error saving data
      this.setState({ isLoading: false })
      console.log('error', error)
      Alert.alert("Error", "Login failed")
    }
  };

  validateIsEmail(email) {
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
  }

  render() {
    const { isLoading, user } = this.state;
    return (
      // <SafeAreaView style={{ flex: 1, }}>
      <Root>
        <Container>

          <StatusBar barStyle='light-content' backgroundColor={Theme.ColorPrimary} />

          <View style={{ alignItems: 'center', marginTop: '5%' }}>

            <Image source={require('../assets/images/SQ_Logo.png')} style={{ width: 120, height: 90, resizeMode: 'contain', alignSelf: 'center' }} />
            {/* <View style={{ flex: 1 }} /> */}
          </View>

          <Content>

            <View
              style={{
                width: "100%",
                justifyContent: "center",
                alignSelf: "center",
                //marginTop: "30%",
                padding: "5%",
              }}
            >

              <Text style={styles.title}>Sign In</Text>

              <View
                style={[
                  Theme.commonStyle.inputWrapper,
                  this.state.emailFocused && { borderColor: Theme.ColorAccent }
                ]}
              >
                <Input
                  placeholder="E-mail"
                  keyboardType="email-address"
                  disabled={isLoading}
                  onChangeText={text => this.setState({ email: text })}
                  value={this.state.email}
                  onFocus={e => this.setState({ emailFocused: true })}
                  onBlur={e => this.setState({ emailFocused: false })}
                  style={Theme.commonStyle.inputTxt}
                />
                <Icon
                  name="envelope"
                  type='EvilIcons'
                  style={Theme.commonStyle.inputIcon}
                />
              </View>

              <View
                style={[
                  Theme.commonStyle.inputWrapper,
                  this.state.passwordFocused && { borderColor: Theme.ColorAccent }
                ]}
              >
                <Input
                  placeholder="Password"
                  keyboardType="default"
                  disabled={isLoading}
                  onChangeText={text => this.setState({ password: text })}
                  value={this.state.password}
                  onFocus={e => this.setState({ passwordFocused: true })}
                  onBlur={e => this.setState({ passwordFocused: false })}
                  secureTextEntry
                  style={[Theme.commonStyle.inputTxt, { marginBottom: 0 }]}
                />
                <Icon
                  name="key"
                  type="AntDesign"
                  style={Theme.commonStyle.inputIcon}
                />
              </View>

              <View
                style={[
                  Theme.commonStyle.inputWrapper,
                  //this.state.emailFocused && { borderColor: Theme.ColorAccent }
                ]}
              >
                <Picker
                  note
                  selectedValue={this.state.site}
                  style={{ height: 50, flex: 1, color: '#808080' }}
                  onValueChange={(itemValue, itemIndex) => {
                    this.setState({
                      site: itemValue,
                      site_id: itemValue
                    })
                  }}
                >
                  <Picker.Item label="Site" value="" />
                  {this.state.sites.map((site, index) => {
                    return <Picker.Item key={"i_" + index} label={site.Name} value={site.Id} />
                  })}
                </Picker>
              </View>

              <Button
                transparent
                onPress={() => alert("working on it...")}
                style={{ marginTop: -20 }}
              >
                <Text
                  style={{
                    alignSelf: "flex-end",
                    fontSize: 12,
                    color: Theme.ColorPrimary,
                    marginLeft: -10
                  }}
                >
                  Forgot Password ?
              </Text>
              </Button>

              <GradientButton
                title="Sign In"
                containerStyle={{
                  alignSelf: "center",
                  marginTop: "15%",
                  marginBottom: "10%"
                }}
                textStyle={{ padding: 10 }}
                isLoading={isLoading}
                onPress={this.OnSubmit}
              />

              {/* <Button
                transparent
                onPress={() => this.props.navigation.navigate("Registration")}
                style={{
                  alignSelf: "center",
                  alignItems: "center",
                  justifyContent: "center",
                  marginTop: "10%"
                }}
              >
                <Text style={styles.textLight}>
                  Don't have account?
                <Text style={[styles.textLight, { color: Theme.ColorPrimary }]}>
                    {" "}
                  Create Account
                </Text>
                </Text>
              </Button> */}
            </View>
          </Content>
        </Container >
      </Root>
      // </SafeAreaView>
    );
  }

  // TODO: test this
  login() {
    Alert.alert("Sorry", "Working on it..")
    // const { email, password } = this.state;

    // if (email.trim().length < 1) {
    //   this.show("Please enter a valid email address");
    //   return;
    // }

    // if (password.trim().length < 1) {
    //   this.show("Please enter your password");
    //   return;
    // }

    // const data = {
    //   UserNameOrEmailAddress: email,
    //   password: password,
    //   rememberClient: true,
    //   tenancy_name: ""
    // };
    // this.setState({
    //   isLoading: true
    // });

    // api
    //   .login(data)
    //   .then(response => {
    //     console.log("response", response);
    //     if (response.success) {
    //       Preferences.saveLoginData(data);
    //       this.props.navigation.navigate("Home", { data: response.result });
    //     } else {
    //       this.show(
    //         response.msg ? response.msg : "Invalid response from server"
    //       );
    //     }
    //     this.setState({
    //       isLoading: false
    //     });
    //   })
    //   .catch(error => {
    //     console.log("error", error);
    //     this.setState({
    //       isLoading: false
    //     });
    //   });
  }

  show(msg) {
    Toast.show({
      text: msg,
      buttonText: "Okay",
      type: 'success'
    });
  }
}

const styles = StyleSheet.create({
  title: {
    color: Theme.ColorPrimary,
    fontSize: 18,
    flex: 1,
    marginTop: '10%',
    marginBottom: 20
  },
  textLight: {
    fontSize: 10,
    color: "gray"
  },
  fee: {
    fontSize: 10,
    color: Theme.ColorPrimary
  }
});
