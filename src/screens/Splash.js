import React, { Component } from 'react';
import { View, Text, Image, ActivityIndicator, StatusBar } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Theme } from "../common/theme";
import Preferences from "../utils/Preferences";
import Api from '../apis/api';
import store from "../store";

const api = new Api("");
export default class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
    // Preferences.getLoginData()
    // .then(response => {
    //   if(response && response.accessToken && response.accessToken.trim().length() > 0){

    //   }
    // })
    // .catch(error => {
    //   console.log('error', error)
    // });

    console.log("Splash mounted")
    this.startTime = new Date().getTime();
    // this.goto('Home');

    Preferences.getLoginData()
      .then(data => {
        console.log('login-data', data);
        if (data && data != null && data != '') {
          store.isLogin = true
          store.user = data
          this.goto('Home', undefined);
        }
        else {
          store.isLogin = false
          store.user = {}
          this.goto('Login', undefined)
        }
      })
      .catch(error => {
        console.log('error', error);
        //this.goto('Login', undefined)
      })
  }

  goto(nextScreen, data) {
    let timeDiff = 2500 - (new Date().getTime() - this.startTime);
    console.log('time-diff', timeDiff)
    if (timeDiff < 0) {
      timeDiff = 0;
    }
    setTimeout(
      () => {
        console.log("launching", nextScreen)
        this.props.navigation.navigate(nextScreen, { data: data })
      },
      timeDiff
    )
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar barStyle="light-content" backgroundColor={Theme.ColorPrimary} translucent />
        <LinearGradient
          start={{ x: 1, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={{ flex: 1 }}
          style={{ flex: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center', }}
          colors={Theme.gradient}>

          {/* Fade-in anim: https://www.youtube.com/watch?v=vzPmI0GCDPM */}
          <Image
            style={{ width: 120, height: 90, marginBottom: 30 }}
            resizeMode='contain'
            fadeDuration={500}
            source={require('../assets/images/SQ_logo_black.png')} />
          <ActivityIndicator color="#fff" />

        </LinearGradient>
      </View>
    );
  }


  doLoginWith(data) {
    api.login(data)
      .then(response => {
        console.log('response', response);
        // TODO: pass data here
        this.goto(response.success ? 'Home' : 'Login', response.result);
      })
      .catch(error => {
        console.log('error', error);
        this.goto('Login', undefined);
      });
  }
}
