import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform, FlatList, TouchableOpacity, Alert } from 'react-native';
import LinearGradient from "react-native-linear-gradient";

import { Button } from "native-base";
import Preferences from "../utils/Preferences";

import { Theme } from "../common/theme";
import { Thumbnail, Icon, Row } from 'native-base';
// import { observer } from 'mobx-react';
import store from '../store';
import { AsyncStorage } from 'react-native';

import { userContext } from '../store/LoginContext';

const HOME = 0;
const PROFILE = 1;
const ABOUT_US = 2;
const CONTACT = 3;
const LOGIN = 4;
const APPOINTMENTS = 5;

// @observer
export default class DrawerSidebar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {},
        };

    }


    render() {

        return (

            <View style={styles.container}>
                <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    style={styles.header}
                    colors={Theme.gradient} >

                    <View style={styles.headerContent}>
                        <Thumbnail large source={require('../assets/images/evSTio.png')} />
                        <Text style={styles.profileName}>{store.user.Name}</Text>
                        <Text style={styles.profDetails}>{store.user.Email}</Text>
                    </View>





                </LinearGradient>

                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', height: 40, paddingLeft: 10, marginTop: 15 }}>

                    <Icon type="AntDesign" name={"clockcircleo"} style={{fontSize: 22}} />
                    <Text style={{ fontSize: 16, marginLeft: 10 }}>Today's Appointments</Text>

                </TouchableOpacity>

                <View style={{ backgroundColor: '#eee', width: '100%', height: 1, marginTop: 10 }} />

                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', height: 40, paddingLeft: 10, marginTop: 5 }}>

                    <Icon type="EvilIcons"  name={"search"} />
                    <Text style={{ fontSize: 16, marginLeft: 10 }}>Search Appointments</Text>

                </TouchableOpacity>
            </View>
        );
    }


}


const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        paddingTop: (Platform.OS == 'ios') ? 20 : 0,
        width: 320,
        height: '100%'
    },
    header: {
        height: 200,
        width: '100%'
    },
    headerContent: {
        width: '100%',
        height: 200,
        justifyContent: 'center',
        alignItems: 'center',
    },
    profileName: {
        color: "#fff",
        fontSize: 18,
        fontWeight: '500',
        marginTop: 10,
    },
    profDetails: {
        color: "#fff",
        fontSize: 12,
        // fontWeight: '500',
        marginTop: 5,
    },
    icon: {
        color: 'gray',
        padding: 10,
        marginLeft: 10,
    },
    title: {
        flex: 1,
        alignSelf: 'center',
        color: 'gray',
    },
})


