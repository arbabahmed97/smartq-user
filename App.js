import React, { Component } from 'react';
import { createBottomTabNavigator, createDrawerNavigator, createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack'
import { Root } from "native-base";

import Splash from "./src/screens/Splash";
import Login from "./src/screens/Login";
import Appointments from "./src/screens/Appointments";


import { Config } from "./src/common/config";

const noHeader = {
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
};


const AppStack = createStackNavigator({
  //Login,
  Appointments
}, noHeader);

const LoginStack = createStackNavigator({
  Login,
}, noHeader);

const AppNavigation = createAppContainer(createSwitchNavigator({
  Splash,
  Login: LoginStack,
  // Login: LoginRegistrationStack,
  Home: AppStack
}, noHeader));

export default class App extends Component {
  render() {
    return (
      <Root>
        <AppNavigation />
      </Root>
    );
  }
}


// Improve specialty and doctor lists?

